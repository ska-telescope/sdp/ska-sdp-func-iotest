
#include <pybind11/pybind11.h>
#define NPY_NO_DEPRECATED_API 1
#include <numpy/arrayobject.h>
#include <arrow/python/pyarrow.h>
#include <arrow/python/numpy_convert.h>
#include <ska-sdp-func/internal/tensor.h>
#include <stdexcept>

namespace pybind11 { namespace detail {

    // Annoyingly, we cannot use arrow's type_names for naming
    // PyBind11's type casters, because they are const char *, and
    // PyBind11 wants to know their size statically. So yeah, we need
    // to (re-)list all supported types here...
    template <typename TYPE> struct arrow_type_name { };
#define ARROW_TYPE_NAME(TYPE, tname)                         \
    template <> struct arrow_type_name<arrow::TYPE##Type> {  \
        static constexpr auto name = _(#tname);              \
    };
    ARROW_TYPE_NAME(HalfFloat, halffloat)
    ARROW_TYPE_NAME(Float, float)
    ARROW_TYPE_NAME(Double, double)
    ARROW_TYPE_NAME(Int8, int8)
    ARROW_TYPE_NAME(Int16, int16)
    ARROW_TYPE_NAME(Int32, int32)
    ARROW_TYPE_NAME(Int64, int64)
    ARROW_TYPE_NAME(UInt8, uint8)
    ARROW_TYPE_NAME(UInt16, uint16)
    ARROW_TYPE_NAME(UInt32, uint32)
    ARROW_TYPE_NAME(UInt64, uint64)

    template <typename TYPE>
    struct type_caster<std::shared_ptr<arrow::NumericTensor<TYPE> > > {
    public:
        PYBIND11_TYPE_CASTER(std::shared_ptr<arrow::NumericTensor<TYPE> >,
                             _("arrow.Tensor[") + arrow_type_name<TYPE>::name + _("]"));

        /// Unwrap arrow tensor object from a pyarrow tensor
        PYBIND11_NOINLINE bool load_tensor(PyObject *source) {
            // Unwrap tensor
            auto tensor_ptr_result = arrow::py::unwrap_tensor(source);
            if (!tensor_ptr_result.ok()) {
                throw std::runtime_error(tensor_ptr_result.status().ToString());
            }
            auto tensor_ptr = tensor_ptr_result.ValueUnsafe();
            // Check type
            if (tensor_ptr->type_id() != TYPE::type_id)
                return false;
            // Cast, return
            value = std::move(std::static_pointer_cast<arrow::NumericTensor<TYPE> >(std::move(tensor_ptr)));
            return true;
        }

        /// Load a Numpy array without prior type check
        PYBIND11_NOINLINE bool load_ndarray_direct(PyArrayObject *ndarray) {
            // Make tensor
            std::shared_ptr<arrow::Tensor> tensor_ptr;
            arrow::Status status = arrow::py::NdarrayToTensor(nullptr,
                                                              reinterpret_cast<PyObject *>(ndarray),
                                                              {}, &tensor_ptr);
            if (!status.ok()) {
                throw std::runtime_error(status.ToString());
            }
            // Re-check type (just to be sure)
            if (tensor_ptr->type_id() != TYPE::type_id) {
                return false;
            }
            // Cast, return
            value = std::move(std::static_pointer_cast<arrow::NumericTensor<TYPE> >(std::move(tensor_ptr)));
            return true;
        }

        /// Load a Numpy array, checking types and adjusting for
        /// complex number types if appropriate
        PYBIND11_NOINLINE bool load_ndarray(PyArrayObject *ndarray) {
            // Complex?
            PyArray_Descr *descr = ndarray->descr;
            bool is_complex = false;
            if (descr->type_num == NPY_COMPLEX64) {
                is_complex = true;
                descr = PyArray_DescrFromType(NPY_FLOAT32);
            } else if(descr->type_num == NPY_COMPLEX128) {
                is_complex = true;
                descr = PyArray_DescrFromType(NPY_FLOAT64);
            }
            // Check type
            std::shared_ptr<arrow::DataType> data_type;
            const arrow::Status status = arrow::py::NumPyDtypeToArrow(descr, &data_type);
            if (!status.ok()) {
                throw std::runtime_error(status.ToString());
            }
            if (data_type->id() != TYPE::type_id) {
                return false;
            }
            // Not complex? Load directly
            if (!is_complex) {
                return load_ndarray_direct(ndarray);
            }
            // Add complex pseudo-axis to dimensions + strides
            std::vector<npy_intp> dimensions(ndarray->nd+1), strides(ndarray->nd+1);
            for (int i = 0; i < ndarray->nd; i++) {
                dimensions[i] = ndarray->dimensions[i];
                strides[i] = ndarray->strides[i];
            }
            dimensions[ndarray->nd] = 2;
            strides[ndarray->nd] = descr->elsize;
            // Create temporary numpy array object of non-complex type
            auto ndarray_ncpx = reinterpret_cast<PyArrayObject*>(PyArray_NewFromDescr(
                &PyArray_Type, descr, ndarray->nd+1,
                dimensions.data(), strides.data(), ndarray->data,
                ndarray->flags, NULL));
            if (!ndarray_ncpx)
                return false;
            // Tie to original ndarray object so the underlying data
            // stays alive
            Py_INCREF(reinterpret_cast<PyObject*>(ndarray));
            PyArray_SetBaseObject(ndarray_ncpx, reinterpret_cast<PyObject*>(ndarray));
            return load_ndarray_direct(ndarray_ncpx);
        }

        /// Attempts to convert arbitrary Python object into tensor
        bool load(handle src, bool convert) {
            // Arrow tensor?
            PyObject *source = src.ptr();
            if (arrow::py::is_tensor(source)) {
                return load_tensor(source);
            }
            // Numpy array?
            if (convert && PyArray_Check(source)) {
                return load_ndarray(reinterpret_cast<PyArrayObject*>(source));
            }
            return false;
        }

        static handle cast(std::shared_ptr<arrow::NumericTensor<TYPE> > src,
                           return_value_policy /* policy */, handle /* parent */) {
            return arrow::py::wrap_tensor(src);
        }

    };

}} // namespace pybind11::detail
