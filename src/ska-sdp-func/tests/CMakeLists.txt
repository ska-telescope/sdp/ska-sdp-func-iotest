add_executable(ska-sdp-func-test test.cc)
target_link_libraries(ska-sdp-func-test ska-sdp-func GTest::GTest GTest::Main)
gtest_add_tests(
    TARGET ska-sdp-func-test
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    TEST_PREFIX ska-sdp-func.
)
