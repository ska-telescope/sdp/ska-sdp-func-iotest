
#include "gtest/gtest.h"
#include "ska-sdp-func/grid/uvw_grid.h"
#include "ska-sdp-func/fft/wshift_fft.h"
#include "ska-sdp-func/internal/hdf5.h"

#include <optional>

using namespace ska_sdp_func;

namespace {

// The fixture for testing class uvw_grid
class uvw_grid_test : public ::testing::Test {
protected:

    // TODO: Test for multiple different grid sizes, strides etc
    const int GRID_SIZE = 12;
    const int GRID_STRIDE = 16;
    const int STEPS = 4;
    const int CHANNELS = 4;

    // Grid resolutions
    const double DU = 1;
    const double DV = 1;
    const double DW = 3;

    std::shared_ptr<ska_sdp_func::uvw_grid> gridder;
    std::shared_ptr<ska_sdp_func::wshift_fft> wshift_fft;

    arrow::Result<std::shared_ptr<arrow::NumericTensor<arrow::DoubleType> > > AllocateGrid(int w_size) {
        int64_t elem_size = sizeof(double);
        return AllocateTensor(
            arrow::DoubleType(),
            { w_size, GRID_SIZE, GRID_SIZE, 2 },
            { GRID_STRIDE*GRID_STRIDE*2*elem_size,
              GRID_STRIDE*2*elem_size, 2*elem_size, elem_size },
            { dim::w, dim::v, dim::u, dim::cpx });
    }

    void SetUp() override {

        // Read gridding function and correction from HDF5, intialise gridder
        // processing component
        auto grid = read_tensor_from_hdf5(
            arrow::DoubleType(), false, {dim::ov, dim::u},
            "data/grid/kernel_pswf_8.h5", "sepkern/kern").ValueOrDie();
        auto corr = read_tensor_from_hdf5(
            arrow::DoubleType(), false, {dim::l},
            "data/grid/kernel_pswf_8.h5", "sepkern/corr").ValueOrDie();
        gridder = std::make_shared<ska_sdp_func::uvw_grid>(
	    DU, DV, DW, grid, corr, grid, corr);

        // Plan FFT using a (temporary) example grid
        wshift_fft = std::make_shared<ska_sdp_func::wshift_fft>(
	    DU, DV, DW, AllocateGrid(1).ValueOrDie());
    }

};


// Test that trivial gridding works
TEST_F(uvw_grid_test, uvw_grid_all_ones) {

    // Make grid buffer
    auto uvw_ld = AllocateTensor(
        arrow::DoubleType(), { STEPS, 6 }, {}, { dim::step, dim::ld }).ValueOrDie();
    auto channels = AllocateTensor(
        arrow::Int64Type(), { STEPS, 2 }, {}, { dim::step, dim::range }).ValueOrDie();
    auto grid = AllocateGrid(GRID_SIZE).ValueOrDie();
    auto vis_out = AllocateTensor(
        arrow::DoubleType(), { STEPS, CHANNELS, 2 }, {}, {dim::step, dim::ch, dim::cpx}).ValueOrDie();

    // Test simple pattern. By setting the margin to completely different
    // values we can at least verify where we run into them.
    for (int u = 0; u < GRID_SIZE; u++) {
	for (int v = 0; v < GRID_SIZE; v++) {
	    for (int w = 0; w < GRID_SIZE; w++)  {
		bool border =
		    u == 0 || u + 1 == GRID_SIZE ||
		    v == 0 || v + 1 == GRID_SIZE;
		MutValue(grid, { w,v,u, 0 }) = (border ? -1e3 : 1);
		MutValue(grid, { w,v,u, 1 }) = 0;
	    }
	}
    }

    for (int step = 0; step < STEPS; step++) {
	MutValue(uvw_ld, { step, 0 }) = 0;
	MutValue(uvw_ld, { step, 1 }) = 0;
	MutValue(uvw_ld, { step, 2 }) = 0;
	MutValue(uvw_ld, { step, 3 }) = 0;
	MutValue(uvw_ld, { step, 4 }) = 0;
	MutValue(uvw_ld, { step, 5 }) = 0;
	MutValue(channels, { step, 0 }) = 0;
	MutValue(channels, { step, 1 }) = CHANNELS;
    }
    MutValue(uvw_ld, { 0, 0 }) = -DU * STEPS / 2;
    MutValue(uvw_ld, { 0, 3 }) = DU;
    MutValue(uvw_ld, { 1, 1 }) = -DV * STEPS / 2;
    MutValue(uvw_ld, { 1, 4 }) = DV;
    MutValue(uvw_ld, { 2, 0 }) = DU * (STEPS / 2 - 1);
    MutValue(uvw_ld, { 2, 3 }) = -DU;
    MutValue(uvw_ld, { 3, 1 }) = DV * (STEPS / 2 - 1);
    MutValue(uvw_ld, { 3, 4 }) = -DV;

    // Call degridder
    gridder->degrid(-GRID_SIZE/2, -GRID_SIZE/2, -GRID_SIZE/2,
		    0, false,
		    uvw_ld, channels, grid, 0, vis_out);

    for (int step = 0; step < STEPS; step++) {
	for (int ch = 0; ch < CHANNELS; ch++) {
	    bool border = (step < 2 ? ch == 0 : ch == CHANNELS-1);
	    EXPECT_NEAR(vis_out->Value({step, ch, 0}),
			(border ? vis_out->Value({0, 0, 0}) : 1), 1e-5);
	    EXPECT_NEAR(vis_out->Value({step, ch, 1}), 0, 1e-5);
	}
    }

}

// Test a couple of properties grid correction should have
TEST_F(uvw_grid_test, uvw_grid_correction) {

    // Set image to all ones
    auto image = AllocateTensor(
        arrow::DoubleType(), { GRID_SIZE, GRID_SIZE, 2 },
        {}, { "m", "l", dim::cpx }).ValueOrDie();
    for (int m = 0; m < GRID_SIZE; m++) {
	for (int l = 0; l < GRID_SIZE; l++) {
	    MutValue(image, { m, l, 0 }) = 1;
	    MutValue(image, { m, l, 1 }) = 1;
	}
    }

    // Apply grid correction
    gridder->degrid_correct(-GRID_SIZE/2, -GRID_SIZE/2,
			    0, 1. / GRID_SIZE / DU,
			    0, 1. / GRID_SIZE / DV,
			    image);

    // Check that the centre is left untouched
    EXPECT_NEAR(image->Value({ GRID_SIZE/2, GRID_SIZE/2, 0 }), 1, 1e-8);
    EXPECT_NEAR(image->Value({ GRID_SIZE/2, GRID_SIZE/2, 1 }), 1, 1e-8);

    // Check against when queried for individual points
    complex<double> point; 
    auto point_tensor = arrow::NumericTensor<arrow::DoubleType>::Make(
	std::make_shared<arrow::MutableBuffer>(reinterpret_cast<uint8_t*>(&point), sizeof(complex<double>)),
	{ 1, 1, 2 }, {}, { dim::l, dim::m, dim::cpx }).ValueOrDie();
    for (int m = 1; m < GRID_SIZE; m++) {
	for (int l = 1; l < GRID_SIZE; l++) {

            const double _l = double(l - GRID_SIZE/2) / GRID_SIZE / DU;
            const double _m = double(m - GRID_SIZE/2) / GRID_SIZE / DV;

	    point = complex<double>(1.0, 1.0);
	    gridder->degrid_correct(0, 0, _l, 0, _m, 0, point_tensor);
	    EXPECT_NEAR(image->Value({ m, l, 0 }), real(point), 1e-9)
		<< "(for m=" << _m << ", l=" << _l << ")";
	    EXPECT_NEAR(image->Value({ m, l, 1 }), imag(point), 1e-9)
		<< "(for m=" << _m << ", l=" << _l << ")";
	    
	}
    }
    
}
    
// Test gridding correctness with point sources
TEST_F(uvw_grid_test, uvw_grid) {

    // Make grid buffer
    auto uvw_ld = AllocateTensor(
        arrow::DoubleType(), { STEPS, 6 }, {}, { dim::step, dim::ld }).ValueOrDie();
    auto channels = AllocateTensor(
        arrow::Int64Type(), { STEPS, 2 }, {}, { dim::step, dim::range }).ValueOrDie();
    auto grid = AllocateGrid(GRID_SIZE).ValueOrDie();
    auto vis_out = AllocateTensor(
        arrow::DoubleType(), { STEPS, CHANNELS, 2 }, {}, {dim::step, dim::ch, dim::cpx}).ValueOrDie();

    // Initialise UVWs
    for (int step = 0; step < STEPS; step++) {
	MutValue(uvw_ld, { step, 0 }) = 0;
	MutValue(uvw_ld, { step, 1 }) = 0;
	MutValue(uvw_ld, { step, 2 }) = 0;
	MutValue(uvw_ld, { step, 3 }) = 0;
	MutValue(uvw_ld, { step, 4 }) = 0;
	MutValue(uvw_ld, { step, 5 }) = 0;
	MutValue(channels, { step, 0 }) = 0;
	MutValue(channels, { step, 1 }) = CHANNELS;
    }
    MutValue(uvw_ld, { 0, 3 }) = 0.1;
    MutValue(uvw_ld, { 1, 4 }) = 0.1;
    MutValue(uvw_ld, { 2, 3 }) = -0.1;
    MutValue(uvw_ld, { 3, 4 }) = -0.1;

    // As well as a tensor referencing a single complex variable, for applying
    // grid correction later
    complex<double> point(1.); 
    auto point_tensor = arrow::NumericTensor<arrow::DoubleType>::Make(
	std::make_shared<arrow::MutableBuffer>(reinterpret_cast<uint8_t*>(&point), sizeof(complex<double>)),
	{ 1, 1, 2 }, {}, { "l", "m", dim::cpx }).ValueOrDie();
    
    // Loop through positions of test source
    const double ls[] = { 0.0, 0.1, 0.3, -0.1, -0.25, 0.3 };
    const double ms[] = { 0.0, 0.0, 0.0, 0.3, 0.1, 0.1 };
    for (size_t ix = 0; ix < sizeof(ls) / sizeof(*ls); ix++) {
	
	// Apply correction
	point = 1.0;
	gridder->degrid_correct(0, 0, ls[ix], 0, ms[ix], 0, point_tensor);

	// Calculate grid
	const double n = sqrt(1 - ls[ix] * ls[ix] - ms[ix] * ms[ix]) - 1;
	const int64_t GRID_W_OFFSET = -GRID_SIZE / 2;
	for (int u = 0; u < GRID_SIZE; u++) {
	    for (int v = 0; v < GRID_SIZE; v++) {
		for (int w = 0; w < GRID_SIZE; w++)  {
		    double _u = DU * (u - GRID_SIZE / 2);
		    double _v = DV * (v - GRID_SIZE / 2);
		    double _w = DW * (w + GRID_W_OFFSET);
		    double ph = -2 * M_PI * (_u * ls[ix] + _v * ms[ix] + _w * n);
		    
		    MutValue(grid, { w,v,u, 0 }) = real(point) * cos(ph);
		    MutValue(grid, { w,v,u, 1 }) = real(point) * sin(ph);
		}
	    }
	}
	
	// Call degridder
	gridder->degrid(-GRID_SIZE/2, -GRID_SIZE/2, GRID_W_OFFSET,
			0, false,
			uvw_ld, channels, grid, 0, vis_out);

	// Calculate DFT + check
	for (int step = 0; step < STEPS; step++) {
	    for (int ch = 0; ch < CHANNELS; ch++) {
		double _u = uvw_ld->Value({step, 0}) + ch * uvw_ld->Value({step, 3});
		double _v = uvw_ld->Value({step, 1}) + ch * uvw_ld->Value({step, 4});
		double _w = uvw_ld->Value({step, 2}) + ch * uvw_ld->Value({step, 5});
		double ph = -2 * M_PI * (_u * ls[ix] + _v * ms[ix] + _w * n);
		EXPECT_NEAR(vis_out->Value({step, ch, 0}), cos(ph), 1.6e-3)
		    << "(l=" << ls[ix] << ", m=" << ms[ix]
		    << ", step=" << step << ", ch=" << ch << ")";
		EXPECT_NEAR(vis_out->Value({step, ch, 1}), sin(ph), 1.6e-3)
		    << "(l1=" << ls[ix] << ", m1=" << ms[ix]
		    << ", step=" << step << ", ch=" << ch << ")";
	    }
	}

    }
}

// Test that trivial FFTs works
TEST_F(uvw_grid_test, fft_simple) {

    auto grid = AllocateGrid(1).ValueOrDie();
    auto image = AllocateTensor(
        arrow::DoubleType(), { GRID_SIZE, GRID_SIZE, 2 }, {}, { dim::m, dim::l, dim::cpx }).ValueOrDie();

    // Zero it
    for (int m = 0; m < GRID_SIZE; m++) {
        for (int l = 0; l < GRID_SIZE; l++) {
            MutValue(image, { m, l, 0 }) = 0;
            MutValue(image, { m, l, 1 }) = 0;
        }
    }

    // Check that result is all zeroes
    wshift_fft->fft(0, image, grid);
    for (int v = 0; v < GRID_SIZE; v++) {
        for (int u = 0; u < GRID_SIZE; u++) {
            EXPECT_NEAR(grid->Value({0, v, u, 0}), 0, 1e-15);
            EXPECT_NEAR(grid->Value({0, v, u, 1}), 0, 1e-15);
        }
    }

    // Put one in the middle, should be all ones now
    MutValue(image, { GRID_SIZE/2,GRID_SIZE/2, 0 }) = 1;
    wshift_fft->fft(0, image, grid);
    for (int v = 0; v < GRID_SIZE; v++) {
        for (int u = 0; u < GRID_SIZE; u++) {
            EXPECT_NEAR(grid->Value({0, v, u, 0}), 1, 1e-15);
            EXPECT_NEAR(grid->Value({0, v, u, 1}), 0, 1e-15);
        }
    }

    // Check that it also works with an imaginary number (linearity)
    MutValue(image, { GRID_SIZE/2,GRID_SIZE/2, 1 }) = -2;
    wshift_fft->fft(0, image, grid);
    for (int v = 0; v < GRID_SIZE; v++) {
        for (int u = 0; u < GRID_SIZE; u++) {
            EXPECT_NEAR(grid->Value({0, v, u, 0}), 1, 1e-15);
            EXPECT_NEAR(grid->Value({0, v, u, 1}), -2, 1e-15);
        }
    }

    // Check highest frequency and axis distinction
    MutValue(image, { GRID_SIZE/2,GRID_SIZE/2, 0 }) = 0;
    MutValue(image, { GRID_SIZE/2,GRID_SIZE/2, 1 }) = 0;
    MutValue(image, { 0, GRID_SIZE/2, 0 }) = 1;
    MutValue(image, { GRID_SIZE/2, 0, 1 }) = 1;
    wshift_fft->fft(0, image, grid);
    for (int v = 0; v < GRID_SIZE; v++) {
        for (int u = 0; u < GRID_SIZE; u++) {
            double real_expected = 1 - 2 * ((v+GRID_SIZE/2) % 2);
            EXPECT_NEAR(grid->Value({0, v, u, 0}), real_expected, 1e-15)
                << "(u = " << u << ", v = " << v << ")";
            double imag_expected = 1 - 2 * ((u+GRID_SIZE/2) % 2);
            EXPECT_NEAR(grid->Value({0, v, u, 1}), imag_expected, 1e-15)
                << "(u = " << u << ", v = " << v << ")";
        }
    }

    // Check half-frequency (and positive axis direction)
    MutValue(image, { 0, GRID_SIZE/2, 0 }) = 0;
    MutValue(image, { GRID_SIZE/2, 0, 1 }) = 0;
    MutValue(image, { GRID_SIZE/2, 3*GRID_SIZE/4, 0 }) = 1;
    MutValue(image, { 3*GRID_SIZE/4, GRID_SIZE/2, 1 }) = 2;
    wshift_fft->fft(0, image, grid);
    for (int v = 0; v < GRID_SIZE; v++) {
        for (int u = 0; u < GRID_SIZE; u++) {
            const complex<double> REF_VALS[] = { 1, -I, -1, I };
            complex<double> expected =
                REF_VALS[(u+GRID_SIZE/2) % 4] +
                std::complex<double>(0.,2.) * REF_VALS[(v+GRID_SIZE/2) % 4];
            EXPECT_NEAR(grid->Value({0, v, u, 0}), real(expected), 1e-15)
                << "(u = " << u << ", v = " << v << ")";
            EXPECT_NEAR(grid->Value({0, v, u, 1}), imag(expected), 1e-15)
                << "(u = " << u << ", v = " << v << ")";
        }
    }

    // Check filled image
    for (int m = 0; m < GRID_SIZE; m++) {
        for (int l = 0; l < GRID_SIZE; l++) {
            MutValue(image, {m, l, 0 }) = 1;
            MutValue(image, {m, l, 1 }) = 0;
        }
    }
    wshift_fft->fft(0, image, grid);
    for (int v = 0; v < GRID_SIZE; v++) {
        for (int u = 0; u < GRID_SIZE; u++) {
            double real_expected = (u == GRID_SIZE/2 && v == GRID_SIZE/2 ? GRID_SIZE * GRID_SIZE : 0);
            EXPECT_NEAR(grid->Value({0, v, u, 0}), real_expected, 1e-15);
            EXPECT_NEAR(grid->Value({0, v, u, 1}), 0, 1e-15);
        }
    }

}

// Check FFT against DFT. This also checks the w-shift.
TEST_F(uvw_grid_test, fft_dft) {

    auto image = AllocateTensor(
        arrow::DoubleType(), { GRID_SIZE, GRID_SIZE, 2 }, {}, { dim::m, dim::l, dim::cpx }).ValueOrDie();
    auto grid = AllocateGrid(GRID_SIZE).ValueOrDie();

    // Loop through all points we could set
    for (int m1 = 0; m1 < GRID_SIZE; m1++) {
        for (int l1 = 0; l1 < GRID_SIZE; l1++) {

            // Set exactly one point to one
            for (int m = 0; m < GRID_SIZE; m++) {
                for (int l = 0; l < GRID_SIZE; l++) {
                    MutValue(image, { m, l, 0 }) = 0;
                    MutValue(image, { m, l, 1 }) = 0;
                }
            }
            MutValue(image, { m1, l1, 0 }) = 1;

            // FFT
            const int64_t GRID_W_OFFSET = -GRID_SIZE/2;
            wshift_fft->fft(GRID_W_OFFSET, image, grid);

            // Calculate (l,m,n) coordinates of the point we just set
            // given the grid resolution
            const double _l1 = double(l1 - GRID_SIZE/2) / GRID_SIZE / DU;
            const double _m1 = double(m1 - GRID_SIZE/2) / GRID_SIZE / DV;
            const double _n1 = sqrt(1 - _l1 * _l1 - _m1 * _m1) - 1;

            // Check all grid points against direct Fourier transform
            for (int w = 0; w < GRID_SIZE; w++) {
                for (int v = 0; v < GRID_SIZE; v++) {
                    for (int u = 0; u < GRID_SIZE; u++) {

                        // Phasor is 2 pi times the dot product of (u,v,w) and (l,m,n)
                        double _u = DU * (u - GRID_SIZE / 2);
                        double _v = DV * (v - GRID_SIZE / 2);
                        double _w = DW * (w + GRID_W_OFFSET);
                        double ph = -2 * M_PI * (_u * _l1 + _v * _m1 + _w * _n1);

                        // Calculate + check
                        EXPECT_NEAR(grid->Value({w, v, u, 0}), cos(ph), 1e-13)
                            << "(l1=" << l1 << ", m1=" << m1
                            << ", u=" << u << ", v=" << v << ", w=" << w << ")";
                        EXPECT_NEAR(grid->Value({w, v, u, 1}), sin(ph), 1e-13)
                            << "(l1=" << l1 << ", m1=" << m1
                            << ", u=" << u << ", v=" << v << ", w=" << w << ")";
                    }
                }
            }
        }
    }
}

// Check gridding+FFT
TEST_F(uvw_grid_test, fft_gridding) {

    auto image = AllocateTensor(
        arrow::DoubleType(), { GRID_SIZE, GRID_SIZE, 2 },
        {}, { dim::m, dim::l, dim::cpx }).ValueOrDie();
    auto grid = AllocateGrid(GRID_SIZE).ValueOrDie();

    auto uvw_ld = AllocateTensor(
        arrow::DoubleType(), { STEPS, 6 }, {}, { dim::step, dim::ld }).ValueOrDie();
    auto channels = AllocateTensor(
        arrow::Int64Type(), { STEPS, 2 }, {}, { dim::step, dim::range }).ValueOrDie();
    auto vis_out = AllocateTensor(
        arrow::DoubleType(), { STEPS, CHANNELS, 2 }, {}, {dim::step, dim::ch, dim::cpx}).ValueOrDie();

    for (int step = 0; step < STEPS; step++) {
        MutValue(uvw_ld, { step, 0 }) = 0;
        MutValue(uvw_ld, { step, 1 }) = 0;
        MutValue(uvw_ld, { step, 2 }) = 0;
        MutValue(uvw_ld, { step, 3 }) = 0;
        MutValue(uvw_ld, { step, 4 }) = 0;
        MutValue(uvw_ld, { step, 5 }) = 0;
        MutValue(channels, { step, 0 }) = 0;
        MutValue(channels, { step, 1 }) = CHANNELS;
    }
    MutValue(uvw_ld, { 0, 3 }) = 0.1;
    MutValue(uvw_ld, { 1, 4 }) = 0.1;
    MutValue(uvw_ld, { 2, 3 }) = -0.1;
    MutValue(uvw_ld, { 3, 4 }) = -0.1;

    // Loop through all points we could set
    const double max_error[] = { 1, 0.3, 4e-2, 8e-4, 8e-4, 5e-4,
				 1e-10, 5e-4, 8e-4, 8e-4, 4e-2, 0.3 };
    for (int m1 = 1; m1 < GRID_SIZE; m1++) {
        for (int l1 = 1; l1 < GRID_SIZE; l1++) {

	    // Determine maximum error allowed
	    double max_err = max_error[m1];
	    if (max_error[l1] > max_err)
		max_err = max_error[l1];
	    
            // Set exactly one point to one
            for (int m = 0; m < GRID_SIZE; m++) {
                for (int l = 0; l < GRID_SIZE; l++) {
                    MutValue(image, { m, l, 0 }) = 0;
                    MutValue(image, { m, l, 1 }) = 0;
                }
            }
            MutValue(image, { m1, l1, 0 }) = 1;

            // Apply grid correction
            gridder->degrid_correct(-GRID_SIZE/2, -GRID_SIZE/2,
				    0, 1. / GRID_SIZE / DU,
				    0, 1. / GRID_SIZE / DV,
				    image);

            // FFT
            const int64_t GRID_W_OFFSET = -GRID_SIZE/2;
            wshift_fft->fft(GRID_W_OFFSET, image, grid);

            // Calculate (l,m,n) coordinates of the point we just set
            // given the grid resolution
            const double _l1 = double(l1 - GRID_SIZE/2) / GRID_SIZE / DU;
            const double _m1 = double(m1 - GRID_SIZE/2) / GRID_SIZE / DV;
            const double _n1 = sqrt(1 - _l1 * _l1 - _m1 * _m1) - 1;

            // Call degridder
            gridder->degrid(-GRID_SIZE/2, -GRID_SIZE/2, -GRID_SIZE/2,
			    0, false,
			    uvw_ld, channels, grid, 0, vis_out);

            // Check results
            for (int step = 0; step < STEPS; step++) {
                for (int ch = 0; ch < CHANNELS; ch++) {

                    double _u = uvw_ld->Value({step, 0}) + ch * uvw_ld->Value({step, 3});
                    double _v = uvw_ld->Value({step, 1}) + ch * uvw_ld->Value({step, 4});
                    double _w = uvw_ld->Value({step, 2}) + ch * uvw_ld->Value({step, 5});
                    double ph = -2 * M_PI * (_u * _l1 + _v * _m1 + _w * _n1);

                    // Calculate DFT + check
                    EXPECT_NEAR(vis_out->Value({step, ch, 0}), cos(ph), max_err)
                        << "(l1=" << l1 << "(" << _l1 << ")" << ", m1=" << m1 << "(" << _m1 << ")" 
                        << ", step=" << step << ", ch=" << ch << ")";
                    EXPECT_NEAR(vis_out->Value({step, ch, 1}), sin(ph), max_err)
                        << "(l1=" << l1 << "(" << _l1 << ")" << ", m1=" << m1 << "(" << _m1 << ")" 
                        << ", step=" << step << ", ch=" << ch << ")";
		    
                }
            }
        }
    }
}

}  // namespace

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
