
#ifndef SKA_SPD_FUNC_INTERNAL_TENSOR_H
#define SKA_SPD_FUNC_INTERNAL_TENSOR_H

#include <arrow/tensor.h>
#include <stdbool.h>
#include <complex.h>
#include <stddef.h>
#include <stdint.h>
#include <iostream>
#include <stdexcept>

namespace ska_sdp_func {

using std::complex;

/// Standard tensor types
template <typename C_TYPE> struct _to_arrow_type {
    using type = typename arrow::CTypeTraits<C_TYPE>::ArrowType;;
};
template <typename C_TYPE>
  struct _to_arrow_type<complex<C_TYPE> > {
    using type = typename _to_arrow_type<C_TYPE>::type;
  };
template <typename C_TYPE>
  using arrow_type = typename _to_arrow_type<C_TYPE>::type;
template <typename C_TYPE>
  using tensor_ptr = std::shared_ptr<arrow::NumericTensor<arrow_type<C_TYPE> > >;

/// Default dimension names
namespace dim {
const std::string step = "step"; /// steps (generally time), used for visibilities
const std::string ch = "ch"; /// channel number, used for visibilities
const std::string ld = "ld"; /// [u,v,w, du,dv,dw] representation of UVW line (u+n*du, v+n*dv, w+n*dw)
const std::string u = "u"; /// u coordinate in grid space
const std::string v = "v"; /// v coordinate in grid space
const std::string w = "w"; /// w coordinate in grid space
const std::string l = "l"; /// l coordinate in image space
const std::string m = "m"; /// m coordinate in image space
const std::string n = "n"; /// n coordinate in image space
const std::string range = "range"; /// [start, end[ range
const std::string ov = "ov"; /// oversampled coordinate (used for kernels)
const std::string cpx = "cpx"; /// [real, imag] representation for complex number
const std::string bc = "*"; /// Broadcast dimension
}


/** Set default dimension names on a tensor
 *
 * Useful if we anticipate that input tensors might not have all dimensions
 * set.
 *
 * @param tensor     Arrow tensor to modify
 * @param dim_names  Dimension names to set, if appropriate
 */
template <class TYPE>
std::shared_ptr<arrow::NumericTensor<TYPE> >
tensor_default_dim_names(const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor,
                         const std::vector<std::string> &dim_names)
{
    // Already has dimension names?
    if (tensor->dim_names().size()) {
        return tensor;
    }
    // Matches number of dimensions?
    if (tensor->ndim() == static_cast<int64_t>(dim_names.size())) {
        // Not using NumericTensor::Make to work around ARROW-14359
        return std::make_shared<arrow::NumericTensor<TYPE> >(
            tensor->data(), tensor->shape(), tensor->strides(), dim_names);
    }
    // Set dimension names, starting from the right
    std::vector<std::string> new_dim_names(tensor->ndim());
    int offset = tensor->ndim() - dim_names.size();
    for (int i = 0; i < tensor->ndim(); i++) {
        if (i < offset)
            new_dim_names[i] = std::to_string(i);
        else
            new_dim_names[i] = dim_names[i-offset];
    }
    // See above
    return std::make_shared<arrow::NumericTensor<TYPE> >(
        tensor->data(), tensor->shape(), tensor->strides(), new_dim_names);
}

/// Parameter error exception
struct parameter_error : public std::runtime_error
{
    template <typename... Args>
    parameter_error(const std::string &func, const std::string &param, Args&&... args)
        : std::runtime_error(arrow::util::StringBuilder(func, " parameter ", param, ": ",
                                                        std::forward<Args>(args)...)),
          func(func), param(param)
    { }

    const std::string func;
    const std::string param;
};

/** Axis information of a tensor */
template <class TYPE>
struct tensor_dim_info
{
    /// Size of dimension. Meaning that indices 0 to size-1 are valid.
    int64_t size;
    /// Stride of dimension - distance between subsequent array
    /// elements along this axis (measured in element sizes)
    int64_t stride;
    /// Does this dimension actually exist in the tensor?
    bool exists;
    /// Name of the dimension
    std::string dim_name;
    /// Parameter name used for passing the tensor - optional, used in
    /// error messages (see check methods)
    std::string param_name;
    /// Function name the tensor was passed to - optional, used in
    /// error messages (see check methods)
    std::string func_name;

    ~tensor_dim_info() = default;

    /// Operator for calculating array offset along this axis.
    ///
    /// @param ix: Zero-based index along this dimension.
    /// @returns Array index
    inline int64_t operator() (int64_t ix) const {
#ifndef NDEBUG
        assert((ix >= 0 && ix < size) || stride == 0);
#endif
        return ix * stride;
    }

    /// Raises a parameter_error if the dimension does not have the
    /// given size
    void check_size(int64_t expected) const {
        if (size != expected) {
            throw parameter_error(func_name, param_name,
                                  "dimension '", dim_name, "' has size ",
                                  size, ", expected ", expected, "!");
        }
    }

    /// Raises a parameter_error if the dimension of checked tensor does not
    /// have the same size as with another tensor
    template <typename TYPE2>
    void check_equal_size(const tensor_dim_info<TYPE2> &other,
                          const bool allow_newaxis) const {
        if (size != other.size) {

            // A "newaxis" is a with stride 0 - which means that we can access
            // any index along this dimension without causing problems, so the
            // size is essentially arbitrary. The calling code will just have
            // to make sure it never actually uses the size of this dimension.
            if (!allow_newaxis || stride != 0) {
                throw parameter_error(func_name, param_name,
                                      "dimension '", dim_name, "' has size ", size, ", but expected ",
                                      other.size, context_str(other), "!");
            }
        }
    }

    /// Raises a parameter_error if the dimension of checked tensor does not
    /// have the given stride
    void check_stride(int64_t expected) const {
        if (stride != expected) {
            throw parameter_error(func_name, param_name,
                                  "dimension '", dim_name, "' has stride ",
                                  stride, " expected ", expected, "!");
        }
    }

    /// Raises a parameter_error if the dimension of checked tensor does not
    /// have the given size and stride
    void check_size_stride(int64_t exp_size, int64_t exp_stride) const {
        if (size != exp_size || stride != exp_stride) {
            throw parameter_error(func_name, param_name,
                                  "dimension '", dim_name, "' has size ",
                                  size, " stride ",
                                  stride, ", expected size ",
                                  exp_size, " stride ", exp_stride, "!");
        }
    }

    /// Raises a parameter_error if the dimension of checked tensor does not
    /// have the given size and stride
    void check_equal_size_stride(const tensor_dim_info<TYPE> &other) const {
        if (size != other.size || stride != other.stride) {
            throw parameter_error(func_name, param_name,
                                  "dimension '", dim_name, "' has size ",
                                  size, " stride ",
                                  stride, ", expected size ",
                                  other.size, " stride ", other.stride,
                                  context_str(other), "!");
        }
    }

private:

    /// Generate a string that relates another dimension info to this
    /// one (used for error messages)
    template <class TYPE2>
    std::string context_str(const tensor_dim_info<TYPE2> &other) const {
        std::ostringstream str;
        str << " (from";
        if (param_name != other.param_name)
            str << " parameter " << other.param_name;
        if (dim_name != other.dim_name)
            str << " dimension " << other.dim_name;
        if (func_name != other.func_name)
            str << " as passed to " << other.func_name;
        str << ")";
        return str.str();
    }
};

/** Find size and stride of a dimension in an Arrow tensor
 *
 * @param tensor  Arrow tensor to query
 * @param dim_name  Dimension name
 * @param param_name  Parameter name (optional, used in error messages)
 */
template <typename TYPE>
inline struct tensor_dim_info<typename TYPE::c_type>
tensor_dim(const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor,
           const std::string &dim_name,
           const std::string &param_name = "",
           const std::string &func_name = "")
{
    using c_type = typename TYPE::c_type;

    // Find index
    const auto &dim_names = tensor->dim_names();
    auto it = std::find(dim_names.begin(), dim_names.end(), dim_name);
    if (it == dim_names.end()) {
        return { 1, 0, false, dim_name, param_name, func_name };
    }

    // Get size + stride (converted into per-element)
    const int64_t ix = std::distance(dim_names.begin(), it);
    return {
        tensor->shape()[ix],
        tensor->strides()[ix] / static_cast<int64_t>(sizeof(c_type)),
        true, dim_name, param_name, func_name
    };
}

/** Find size and stride of a dimension in an complex-type Arrow tensor
 *
 * @param tensor  Arrow tensor to query
 * @param dim_name  Dimension name
 */
template <typename TYPE>
inline struct tensor_dim_info<complex<typename TYPE::c_type> >
tensor_dim_cpx(const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor,
               const std::string &dim_name,
               const std::string &param_name = "",
               const std::string &func_name = "")
{
#ifndef NDEBUG
    // Quick sanity check: Is the tensor actually complex?
    // This should have been checked before...
    const auto &dim_names = tensor->dim_names();
    assert(std::find(dim_names.begin(), dim_names.end(), dim::cpx) != dim_names.end());
#endif
    const auto dim_info = tensor_dim(tensor, dim_name);
    return {
        dim_info.size,
        dim_info.stride / 2,
        dim_info.exists,
        dim_name, param_name, func_name
    };
}

/** Helper to get type-safe pointer to numeric tensor data
 *
 * @param tensor  Arrow tensor to query
 */
template <typename TYPE>
inline const typename TYPE::c_type *tensor_data(
    const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor)
{
    // Obligatory "no idea why Arrow doesn't offer this"
    return reinterpret_cast<const typename TYPE::c_type *>(tensor->raw_data());
}

/** Helper to get type-safe pointer to numeric complex-valued tensor data
 *
 * @param tensor  Arrow tensor to query
 */
template <typename TYPE>
inline const complex<typename TYPE::c_type> *tensor_data_cpx(
    const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor)
{
    return reinterpret_cast<const complex<typename TYPE::c_type> *>(tensor->raw_data());
}

/** Helper to get type-safe pointer to mutable numeric tensor data
 *
 * @param tensor  Arrow tensor to query
 */
template <typename TYPE>
inline typename TYPE::c_type *tensor_mutable_data(
    const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor)
{
#if ARROW_VERSION_MAJOR < 4
    // Until Arrow 4.0.0, tensors created from numpy arrays might return a
    // null pointer from raw_mutable_data() despite is_mutable() being
    // set. See https://issues.apache.org/jira/browse/ARROW-12495
    assert(tensor->is_mutable());
    uint8_t *mut_data = const_cast<uint8_t *>(tensor->raw_data());
#else
    uint8_t *mut_data = tensor->raw_mutable_data();
#endif
    // Obligatory "no idea why Arrow doesn't offer this"
    return reinterpret_cast<typename TYPE::c_type *>(mut_data);
}

/** Helper to get type-safe pointer to mutable numeric complex-valued tensor data
 *
 * @param tensor  Arrow tensor to query
 */
template <typename TYPE>
inline complex<typename TYPE::c_type> *tensor_mutable_data_cpx(
    const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor)
{
    return reinterpret_cast<complex<typename TYPE::c_type> *>(tensor_mutable_data(tensor));
}

/// Raises a parameter_error if the tensor is not CPU-allocated
template <typename TYPE>
void tensor_check_cpu(const std::string &func_name, const std::string &param_name,
                      const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor) {
    if (!tensor->data()->is_cpu()) {
        throw parameter_error(func_name, param_name, "must be CPU-allocated!");
    }
}

/// Raises a parameter_error if the tensor is not mutable
template <typename TYPE>
void tensor_check_mutable(const std::string &func_name, const std::string &param_name,
                          const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor) {
    if (!tensor->is_mutable()) {
        throw parameter_error(func_name, param_name, "must be mutable!");
    }
}


/// Raises a parameter_error if the tensor is not complex (i.e. has a
/// dim::cpx axis of size 2 and stride 1)
template <typename TYPE>
void tensor_check_complex(const std::string &func_name, const std::string &param_name,
                          const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor) {
    auto dim_info = tensor_dim(tensor, dim::cpx);
    if (!dim_info.exists) {
        throw parameter_error(func_name, param_name,
                              "should be complex (i.e. have dimension 'cpx', "
                              "with size 2 and stride 1)!");
    }
    else if (dim_info.size != 2 || dim_info.stride != 1) {
        throw parameter_error(func_name, param_name,
                              "should be complex ("
                              "dimension 'cpx' should have size 2 and stride 1, but "
                              "found size ", dim_info.size,             \
                              " stride ", dim_info.stride, ")!");
    }
}

/// Dummy tensor_dim_info with extra template parameter used for
/// replicating according to number of arguments
template <class TYPE, typename ARG>
  using _tensor_dim_info = tensor_dim_info<TYPE>;

/// Unpack a tensor in CPU memory
template <typename TYPE, typename... ARGS>
inline std::tuple<const typename TYPE::c_type *, _tensor_dim_info<typename TYPE::c_type, ARGS>...>
tensor_unpack(const std::string &func_name, const std::string &param_name,
              const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor,
              const ARGS&... dim_names)
{
    // Check tensor
    tensor_check_cpu(func_name, param_name, tensor);
    auto tensor_default = tensor_default_dim_names(tensor, { dim_names... });
    // Get data pointer, as unary tuple. Obligatory "no idea why Arrow
    // doesn't do the reinterpret_cast for us".
    auto data = reinterpret_cast<const typename TYPE::c_type *>(tensor->raw_data());
    // Returned tuple with data + dimension information
    return std::make_tuple(data, tensor_dim(tensor_default, dim_names, param_name, func_name)...);
}

/// Unpack a tensor with complex data in CPU memory
template <typename TYPE, typename... ARGS>
inline std::tuple<const complex<typename TYPE::c_type> *,
                  _tensor_dim_info<complex<typename TYPE::c_type>, ARGS>...>
tensor_unpack_cpx(const std::string &func_name, const std::string &param_name, 
                  const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor,
                  const ARGS&... dim_names)
{
    tensor_check_cpu(func_name, param_name, tensor);
    // Default dimensions
    auto tensor_default = tensor_default_dim_names(tensor, { dim_names..., dim::cpx });
    tensor_check_complex(func_name, param_name, tensor_default);
    // Get data pointer, as unary tuple. Obligatory "no idea why Arrow
    // doesn't do the reinterpret_cast for us".
    auto data = reinterpret_cast<const complex<typename TYPE::c_type> *>(tensor->raw_data());
    // Returned tuple with data + dimension information
    return std::make_tuple(data, tensor_dim_cpx(tensor_default, dim_names, param_name, func_name)...);
}

/// Unpack a mutable tensor in CPU memory
template <typename TYPE, typename... ARGS>
inline std::tuple<typename TYPE::c_type *, _tensor_dim_info<typename TYPE::c_type, ARGS>...>
tensor_unpack_mutable(const std::string &func_name, const std::string &param_name, 
                      const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor,
                      const ARGS&... dim_names)
{
    tensor_check_cpu(func_name, param_name, tensor);
    tensor_check_mutable(func_name, param_name, tensor);
    auto tensor_default = tensor_default_dim_names(tensor, { dim_names... });
    // Returned tuple with data + dimension information
    return std::make_tuple(tensor_mutable_data(tensor),
                           tensor_dim(tensor_default, dim_names, param_name, func_name)...);
}

/// Unpack a mutable tensor with complex data in CPU memory
template <typename TYPE, typename... ARGS>
inline std::tuple<complex<typename TYPE::c_type> *,
                  _tensor_dim_info<complex<typename TYPE::c_type>, ARGS>...>
tensor_unpack_mutable_cpx(const std::string &func_name, const std::string &param_name, 
                          const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor,
                          const ARGS&... dim_names)
{
    tensor_check_cpu(func_name, param_name, tensor);
    tensor_check_mutable(func_name, param_name, tensor);
    // Default dimensions
    auto tensor_default = tensor_default_dim_names(tensor, { dim_names..., dim::cpx });
    tensor_check_complex(func_name, param_name, tensor_default);
    // Get data + dimension informations
    return std::make_tuple(tensor_mutable_data_cpx(tensor),
                           tensor_dim_cpx(tensor_default, dim_names, param_name, func_name)...);
}

/// Helper for allocating a tensor
template<typename TYPE>
arrow::Result<std::shared_ptr<arrow::NumericTensor<TYPE> > >
AllocateTensor(const TYPE &typ,
               const std::vector<int64_t>& shape,
               const std::vector<int64_t>& strides,
               const std::vector<std::string>& dim_names)
{

    // Determine required size for buffer
    int bytes = typ.bit_width() / 8;
    if (strides.size() == 0) {
        for (int64_t dim : shape) {
            bytes *= dim;
        }
    } else {
        int max_bytes = 0;
        for (size_t i = 0; i < strides.size(); i++) {
            int64_t bs = bytes * strides[i] * shape[i];
            if (bs > max_bytes) {
                max_bytes = bs;
            }
        }
        bytes = max_bytes;
    }

    // Make buffer, wrap as numeric tensor
    ARROW_ASSIGN_OR_RAISE(std::shared_ptr<arrow::Buffer> buf, arrow::AllocateBuffer(bytes));
    return arrow::NumericTensor<TYPE>::Make(buf, shape, strides, dim_names);
}

/// Dump a tensor
template<typename TYPE>
void DumpTensor(const std::shared_ptr<arrow::NumericTensor<TYPE> > &tensor)
{

    std::cout << "Tensor[";

    // Show dimensions
    int ndim = tensor->ndim();
    for (int i = 0; i < ndim; i++) {
        std::cout << tensor->dim_name(i) << ':' << tensor->shape()[i];
        if (i+1 < ndim)
            std::cout << ',';
    }
    std::cout << "] at 0x" << std::hex << reinterpret_cast<intptr_t>(tensor->raw_data());

    // Dump
    std::vector<int64_t> ix(ndim);
    bool is_complex = false;
    if (ndim > 0 && tensor->dim_name(ndim-1) == dim::cpx && tensor->shape()[ndim-1]) {
        is_complex = true;
        ndim --;
    }
    int64_t nfirst = ndim;
    while(true) {

        // Opening brackets
        if (nfirst) {
            std::cout << std::endl;
            for (int j = 0; j < ndim; j++) {
                std::cout << ((j < ndim - nfirst) ? ' ' : '[');
            }
        }

        // Value
        if (is_complex) {
            typename TYPE::c_type real = tensor->Value(ix);
            ix[ndim] = 1;
            typename TYPE::c_type img = tensor->Value(ix);
            ix[ndim] = 0;
            std::cout << std::complex<double>(real, img);
        } else {
            std::cout << tensor->Value(ix);
        }

        // Increment outer to inner (C order)
        int i = ndim-1;
        while(i >= 0) {
            ix[i]++;
            if (ix[i] < tensor->shape()[i])
                break;
            ix[i] = 0;
            i--;
            std::cout << ']';
        }
        if (i < 0)
            break;
        std::cout << ',';
        nfirst = ndim - 1 - i;

    };
    std::cout << std::endl;

}


// Helper for setting value in tensor (... slow ...)
template <typename TYPE>
typename TYPE::c_type&
MutValue(std::shared_ptr<arrow::NumericTensor<TYPE> > tensor,
         const std::vector<int64_t>& index) {
    assert(tensor->is_mutable());
    return const_cast<typename TYPE::c_type&>(tensor->Value(index)); // NOLINT: cppcoreguidelines-pro-type-const-cast
}

extern "C" {

/** Helper for aligned memory allocation.
 *
 * This is for some reason not exactly portable.
 *
 * @param size  Number of bytes to allocate
 * @param align  Alignment returned value should
 */
void *a_alloc(size_t size, size_t align);
// TODO: Replace by Arrow memory pool allocator?

}
}

#endif // SKA_SPD_FUNC_INTERNAL_TENSOR_H
