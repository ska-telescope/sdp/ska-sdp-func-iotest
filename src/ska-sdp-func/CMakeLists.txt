# Configure a header file to pass some of the CMake settings
# to the source code
configure_file(config.h.in config.h)

# Involved files
set(public_headers
  ${CMAKE_CURRENT_BINARY_DIR}/config.h
  internal/tensor.h internal/hdf5.h
  grid/uvw_grid.h fft/wshift_fft.h
)
set(private_headers
)
set(sources
  grid/uvw_grid.cc fft/wshift_fft.cc
)
set_source_files_properties(grid/uvw_grid.cc PROPERTIES COMPILE_FLAGS "-msse4.2")

# Generated kernels
set(KERNELS ${CMAKE_CURRENT_BINARY_DIR}/codegen)
set(UVWGRID_KERNELS "")
foreach(size IN ITEMS 6_4 8_4 8_6 8_8)
 foreach(arch IN ITEMS fma avx2)
  list(APPEND GRID_FILES ${KERNELS}/griduvw2_${arch}_${size}.h)
  set(UVWGRID_KERNELS "${UVWGRID_KERNELS}#include \"griduvw2_${arch}_${size}.h\"\n")
  add_custom_command(
    OUTPUT ${KERNELS}/griduvw2_${arch}_${size}.h
    DEPENDS codegen/mk_griduvw_avx2_new.py
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/src/ska-sdp-func
    COMMAND ${Python3_EXECUTABLE} codegen/mk_griduvw_avx2_new.py ${KERNELS}/griduvw2_${arch}_${size}.h
    )
 endforeach()
endforeach()

# Generate file including all kernels. Make sure to only write it if
# it actually changed, otherwise this would trigger re-build every
# time you re-run CMake.
if(NOT EXISTS ${KERNELS}/griduvw2_all.h)
  file(WRITE ${KERNELS}/griduvw2_all.h "${UVWGRID_KERNELS}")
else()
  file(READ ${KERNELS}/griduvw2_all.h UVWGRID_KERNELS2)
  if (NOT UVWGRID_KERNELS STREQUAL UVWGRID_KERNELS2)
    file(WRITE ${KERNELS}/griduvw2_all.h "${UVWGRID_KERNELS}")
  endif()
endif()

# Target
add_library(
  ska-sdp-func
    ${sources}
    ${private_headers}
    ${public_headers}
    ${GRID_FILES}
)
set_target_properties(ska-sdp-func PROPERTIES VERSION ${ska_sdp_func_VERSION})
target_include_directories(ska-sdp-func PRIVATE
  ${KERNELS}
)
target_include_directories(ska-sdp-func PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src>
  $<INSTALL_INTERFACE:include>
  ${HDF5_C_INCLUDE_DIRS}
  )
target_link_libraries(ska-sdp-func PUBLIC
  ${ARROW_LIBRARIES} ${HDF5_C_LIBRARIES} ${CMAKE_DL_LIBS}
  PkgConfig::FFTW ZLIB::ZLIB Threads::Threads
  )

if (BUILD_TESTING)
  add_subdirectory(tests)
endif()

install(
  TARGETS
    ska-sdp-func
  EXPORT
    ska-sdp-func-targets
  LIBRARY DESTINATION
    lib
  ARCHIVE DESTINATION
    lib
)

# Add all (public) header files, making sure to install them into the
# appropriate subdirectories so the overall directory structure stays
# the same.
foreach ( file ${public_headers} )
    get_filename_component( dir ${file} DIRECTORY )
    install( FILES ${file} DESTINATION include/ska-sdp-func/${dir} )
endforeach()
