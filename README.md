
# Processing Function Library (I/O test)

This library provides processing functions for the Science Data
Processor. Currently it covers functions extracted from the I/O test,
mainly Fourier transformation and degridding.

## Dependencies (Debian)

At this point, only installation from source is supported. To get
started, make sure you have all dependencies installed - especially a
current version of the Arrow library. To do this, follow the the
[Apache Arrow installation
instructions](https://arrow.apache.org/install/), which currently
requires you to first register custom sources:

    $ sudo apt update
    $ sudo apt install -y -V ca-certificates lsb-release wget
    $ wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
    $ sudo apt install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb

Next you should be able to install the required system dependencies:

    $ sudo apt update
    $ sudo apt install -y libhdf5-dev libfftw3-dev libgtest-dev \
                          libarrow-dev libarrow-python-dev python3 \
                          python3-pip python3-sphinx python3-breathe pybind11-dev \
                          doxygen pkg-config cmake

At this point, you should be able to install the Python depdencies -
preferrably in a virtual Python environment:

    $ pip3 install pyarrow pytest h5py --no-binary=pyarrow
    $ python3 -m pip install git+git://github.com/pybind/pybind11_mkdoc.git@master

It is important to prevent the `pyarrow` wheel from getting used
(`--no-binary`), as otherwise this step might install a version of
Arrow competing with the system one (which can cause linking
problems). If the `pyarrow` installation fails with an error along the
lines of `Could NOT find ArrowPython`, this is because CMake fails to
locate `ArrowPythonConfig.cmake`, which can be worked around:

    $ ln -sf /usr/lib/x86_64-linux-gnu/cmake/arrow /usr/lib/x86_64-linux-gnu/cmake/arrowpython

## Building and Installation

This project is built using CMake. As this is integrated with
`setup.py`, for Python development it should be enough to execute:

    $ python setup.py install

Building the library manually works as usual for CMake:

    $ mkdir build; cd build
    $ cmake ..
    $ make

Especially make sure to run the test cases:

    $ make test

You can test the Python module by adding
`build/src/ska-sdp-func-python` to the `PYTHONPATH`:

    $ PYTHONPATH=$PYTHONPATH:src/ska-sdp-func-python python3
