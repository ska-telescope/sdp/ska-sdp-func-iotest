
FROM artefact.skao.int/ska-cicd-cpp-build-base:0.2.10

# Install system dependencies
RUN apt-get update -y && \
    apt install -y -V ca-certificates lsb-release wget && \
    wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb && \
    apt install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb && \
    apt-get update -y && \
    apt-get install -y libhdf5-dev \
                       libfftw3-dev \
                       libgtest-dev \
                       libarrow-dev \
                       libarrow-python-dev \
                       python3 \
                       python3-pip \
                       python3-sphinx \
                       python3-breathe \
                       pybind11-dev \
                       doxygen \
                       pkg-config git \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

# Install Python dependencies. The first line is a (hopefully temporary) workaround to make
# find_package(ArrowPython CONFIG) work with CMake.
RUN ln -sf /usr/lib/x86_64-linux-gnu/cmake/arrow /usr/lib/x86_64-linux-gnu/cmake/arrowpython && \
    pip3 install -U cppcheck-junit junit-xml pyarrow pytest pip h5py && \
    python3 -m pip install git+git://github.com/pybind/pybind11_mkdoc.git@master

# Copy conversion scripts
COPY clang-tidy-to-junit.py /usr/local/bin/
COPY ctest-to-junit.py /usr/local/bin/
