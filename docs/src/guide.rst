
Implementation Guidelines
=========================

Our goal is to make the SDP processing function library a source of
widely-applicable, high quality radio astronomy algorithm
implementations. We believe that this is instrumental both in
maintaining the viability of the SKA SDP in the face of changing
requirements and tough scaling challenges -- as well as making our own
lives easier and more comfortable in getting there.

For this reason, we would like to hold all processing functions to
quite high standards. Note that there clearly are non-trivial
trade-offs here -- most of these rules are meant to be broken, but we
should not do so carelessly!

Rule 1: Simple, reusable functions
----------------------------------

The easiest way to make sure that an implementation remains useful is
to make minimal assumptions about how and why it is going to get
used. Therefore we should aim to make the interfaces (parameters, data
models) to functions as generic as possible.

* Can a processing function be simplified by splitting its
  functionality into separate processing functions? For instance, a
  common pattern is that we can reduce the number of parameters using
  a new processing function applying a pre-processing step.

* Can we cover more use cases by generalisation, for instance by
  enabling work on partial inputs, or supporting extra axes to the
  input data? The former will come in useful for distributed
  execution, and approaches such as `numpy-style broadcasting
  behaviour
  <https://numpy.org/doc/stable/user/basics.broadcasting.html>`__ can
  greatly increase the applicability of a processing function without
  having to actually specialise it.

Rule 2: Shared, flexible data models
------------------------------------

To make it possible to compose processing functions, they need to be
able to understand the data emitted by each other.

* Is the data represented in a "normal" fashion - i.e. without
  redundancy or extraneous information? For instance, storing
  visibility UVW coordinates individually per channel could be seen as
  redundant where the step length per channel is constant.

* Is the meaning of the data models sufficiently clear without
  unecessary mental or computational steps? For instance, radio
  astronomy Fourier transforms typically consider a "centered" signal
  for images and grids, so this should be the default data model as
  far as possible.

* We should be mindful to keep the number of data models low, so we
  have an easy time documenting them. This especially applies to data
  models that achieve similar purposes, but with slightly different
  conventions applied.

Rule 3: Design for performance
------------------------------

In practice, usefulness of the processing function library will
greatly depend on the speed of the implementation. We clearly don't
want to optimise prematurely, but we should make sure that we have a
path in mind for making our processing functions worth running at SKA
scale.

* Do we have a performance model of our processing function? We should
  have at least a rough understanding of how complexity is going to
  scale, typically depending on the size of the parameters.  Clearly
  we cannot predict with absolute certainty which part will turn out
  to be the bottleneck, but we should aim to identify at least the
  prime suspects of every function - such as expensive operations or
  I/O.

  This performance model should be made explicit: Every processing
  function should provide monitoring points to monitor (say) the
  number of effective floating point operations executed.

* Have we sufficiently optimised the hot spots (e.g. inner loops) for
  configurations relevant to overall system performance? Where
  profiling and comparison against the performance model has produced
  evidence that we may be leaving significant efficiency on the floor,
  we should look into optimisation. A typical approach here should be
  to provide specialised implementations for these cases.

* Are interfaces defined in a way that enables performance? Calling
  the processing function over the interface itself has cost, and we
  must ensure that this will not dominate the cost of processing.  For
  instance, for inherently large data entities (visiblities, images,
  grids) we should avoid copying as much as possible, instead opting
  for passing data in and out of processing functions in terms of
  references to (slices of) existing buffers.


Rule 4: Well-documented implementation
--------------------------------------

The focus of the processing function library should be well-understood
algorithms, with most complexity arising from how they get
combined. However, given the competing requirements for generality and
specialisation, it is likely that the implementation will become quite
involved in places. As we intend the processing funcion library to
have many contributors and users, this means we need to document well
what every processing function is meant to do.

* We should provide documentation that explains what the function
  does, as well as the context it is expected to be used in. Apart
  from API documentation (e.g. Doxygen/Sphinx) this should likely
  cover references to publications or further research in the context
  of SKAO construction.

* Extensive test cases should be provided for every processing
  function, ideally arranged in a way that also incrementally
  documents the properties expected of the function. To decrease
  coupling, definition of function tests in terms of other functions
  or opaque reference data should be minimised.

* Even where we might end up providing optimised versions of the
  functions for all use cases, it is a good idea to keep "reference"
  implementations in the code base - both to provide executeable
  documentation and act as a source of truth for possible corner
  cases.
